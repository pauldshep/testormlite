package sss.com.testormlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;
import java.util.Random;

public class MainActivity extends OrmLiteBaseActivity<DatabaseHelper>
{
    private final static String TAG = "MainActivity";

    private TextView mTextViewMain;
    private Button   mButtonRead;
    private Button   mButtonWrite;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextViewMain = findViewById(R.id.textview_main);
        mButtonRead   = findViewById(R.id.button_read);
        mButtonWrite  = findViewById(R.id.button_write);

        mButtonRead.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                doSampleDatabaseStuff("Read");
            }
        });

        mButtonWrite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                doSampleDatabaseStuff("Write");
            }
        });
    }   // end protected void onCreate(Bundle savedInstanceState)


    /**
     * do sample database stuff
     *
     * @global mTextViewMain
     *
     * @param action reference action
     */
    private void doSampleDatabaseStuff(String action)
    {
        // get the DAO
        RuntimeExceptionDao<SimpleData, Integer> simple_dao =
            getHelper().getSimpleDataDao();

        // query for all of the data objects in the database
        List<SimpleData> list = simple_dao.queryForAll();

        // build the content view
        StringBuilder sb = new StringBuilder();
        sb.append("got ").append(list.size()).append(" entries in ").append(action).append("\n");

        // if we already have items in the database
        int simpleC = 0;
        for (SimpleData simple : list)
        {
            sb.append("------------------------------------------\n");
            sb.append("[").append(simpleC).append("] = ").append(simple).append("\n");
            simpleC++;
        }

        sb.append("------------------------------------------\n");

        for (SimpleData simple : list)
        {
            simple_dao.delete(simple);
            sb.append("deleted id ").append(simple.id).append("\n");
            Log.i(TAG, "deleting simple(" + simple.id + ")");
            simpleC++;
        }

        int createNum;
        do
        {
            createNum = new Random().nextInt(3) + 1;
        }
        while (createNum == list.size());

        for (int i = 0; i < createNum; i++)
        {
            // create a new simple object
            long       millis      = System.currentTimeMillis();
            SimpleData simple_data = new SimpleData(millis);

            // store it in the database
            simple_dao.create(simple_data);
            Log.i(TAG, "created simple(" + millis + ")");

            // output it
            sb.append("------------------------------------------\n");
            sb.append("created new entry #").append(i + 1).append(":\n");
            sb.append(simple_data).append("\n");
            try
            {
                Thread.sleep(5);
            }
            catch (InterruptedException e)
            {
                // ignore
            }
        }

        mTextViewMain.setText(sb.toString());
        Log.i(TAG, "Done with page at " + System.currentTimeMillis());

    }

}   // end public class MainActivity extends OrmLiteBaseActivity<DatabaseHelper>
